// Copyright (c) 2018 Artur Świgoń <aswigon@aswigon.pl>

#include <cstdlib>
#include <iostream>
#include <map>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include <ft2build.h>
#include FT_FREETYPE_H
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <png.h>
#include <zlib.h>

#include "Font.h"
#include "Window.h"

namespace
{
    FT_Library freetype_library;
    GLFWwindow *glfw_window;
    std::ostream &out = std::clog; // Output stream for logging

    template <typename T>
    std::string version(T major, T minor, T patch)
    {
        std::stringstream s;
        s << std::to_string(major) << ".";
        s << std::to_string(minor) << ".";
        s << std::to_string(patch);
        return s.str();
    }

    void run(const std::vector<std::string> &args)
    {
        // Currently unused
        (void)args;

        // Dictionary of library versions, maps 'name' to ('header_version', 'runtime_version')
        std::map<std::string, std::pair<std::string, std::string>> versions;

        // Query libpng and zlib versions
        versions["libpng"].first = PNG_LIBPNG_VER_STRING;
        versions["libpng"].second = png_get_libpng_ver(nullptr);
        versions["zlib"].first = ZLIB_VERSION;
        versions["zlib"].second = zlibVersion();

        // Initialize FreeType and register destructor
        auto freetype_error = FT_Init_FreeType(&freetype_library);
        if (freetype_error)
            throw std::runtime_error("Failed to initialize FreeType");
        std::atexit([]{ FT_Done_FreeType(freetype_library); });

        int ft_major, ft_minor, ft_patch;
        FT_Library_Version(freetype_library, &ft_major, &ft_minor, &ft_patch);
        versions["freetype"].first = version(FREETYPE_MAJOR, FREETYPE_MINOR, FREETYPE_PATCH);
        versions["freetype"].second = version(ft_major, ft_minor, ft_patch);
        gkom::Font::setLibrary(freetype_library);

        // Initialize GLFW and register destructor
        auto glfw_success = glfwInit();
        if (glfw_success == GLFW_FALSE)
            throw std::runtime_error("Failed to initialize GLFW");
        std::atexit([]{ glfwTerminate(); });

        int glfw_major, glfw_minor, glfw_patch;
        glfwGetVersion(&glfw_major, &glfw_minor, &glfw_patch);
        versions["glfw"].first = version(GLFW_VERSION_MAJOR, GLFW_VERSION_MINOR, GLFW_VERSION_REVISION);
        versions["glfw"].second = version(glfw_major, glfw_minor, glfw_patch);

        // Configure and create GLFW window and register its destructor
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_DECORATED, GLFW_TRUE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        glfw_window = glfwCreateWindow(0xFF, 0xFF, "Window", nullptr, nullptr); // Size and title set later
        if (!glfw_window)
            throw std::runtime_error("Failed to create GLFW window");
        glfwHideWindow(glfw_window);
        glfwMakeContextCurrent(glfw_window);
        std::atexit([]{ glfwDestroyWindow(glfw_window); });

        // Initialize GLEW
        auto glew_status = glewInit();
        if (glew_status != GLEW_OK)
            throw std::runtime_error("Failed to initialize GLEW");

        versions["glew"].first = version(GLEW_VERSION_MAJOR, GLEW_VERSION_MINOR, GLEW_VERSION_MICRO);
        versions["glew"].second = reinterpret_cast<const char *>(glewGetString(GLEW_VERSION));

        // Log library versions
        for (auto &version : versions)
        {
            auto name = version.first;
            auto header_ver = version.second.first;
            auto runtime_ver = version.second.second;

            out << "Compiled with " << name << " ";
            out << header_ver << "; using " << runtime_ver << "\n";
        }
        out << "\n";

        // Log OpenGL information
        out << "Using OpenGL " << glGetString(GL_VERSION);
        out << "; on " << glGetString(GL_RENDERER) << "\n";

        // Initialize GKOM window module
        gkom::Window::init(glfw_window, 1280, 720);
        gkom::Window::title("GKOM");
        gkom::Window::show();
        gkom::Window::mainLoop();
    }
}

int main(int argc, char **argv)
{
    out << "GKOM OpenGL showcase program" << "\n";
    out << "Copyright (c) 2018 Artur Swigon <aswigon@aswigon.pl>" << "\n\n";

    std::vector<std::string> args(static_cast<size_t>(argc));
    for (int i = 0; i < argc; ++i)
        args.emplace_back(argv[i]);

    try
    {
        run(args);
    }
    catch (const std::exception &e)
    {
        std::cerr << "error: " << e.what() << std::endl;
        std::exit(1);
    }

    // Exit normally
    return 0;
}
