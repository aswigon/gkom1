// Copyright (c) 2018 Artur Świgoń <aswigon@aswigon.pl>

#ifndef GKOM1_CYLINDER_H
#define GKOM1_CYLINDER_H

#include <GL/glew.h>

#include "Program.h"

namespace gkom
{
    class Cylinder
    {
    public:
        Cylinder() = delete;
        Cylinder(const Cylinder &) = delete;
        Cylinder(Cylinder &&) noexcept = delete;

        Cylinder(gkom::Program &program);
        ~Cylinder() noexcept;

        Cylinder &operator=(const Cylinder &) = delete;
        Cylinder &operator=(Cylinder &&) noexcept = delete;

        gkom::Program &program() const { return _program; }

        void draw();

    private:
        gkom::Program &_program;
        GLuint _ebo;
        GLuint _vao;
        GLuint _vbo;
    };
}

#endif // GKOM1_CYLINDER_H
