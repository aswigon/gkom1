// Copyright (c) 2018 Artur Świgoń <aswigon@aswigon.pl>

#include <array>
#include <initializer_list>
#include <stdexcept>
#include <string>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Program.h"

gkom::Program::Program(std::initializer_list<const gkom::Shader *> list)
{
    auto program = glCreateProgram();
    for (auto shader : list)
        glAttachShader(program, shader->id());

    glLinkProgram(program);

    GLint success;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success)
    {
        const std::size_t bufferSize = 512;
        std::array<GLchar, bufferSize> log{};
        GLsizei len;

        glGetProgramInfoLog(program, bufferSize, &len, log.data());
        std::string msg(static_cast<char *>(log.data()), static_cast<std::size_t>(len));
        throw std::runtime_error("Failed to link program:\n" + msg);
    }

    _program = program;
}

gkom::Program::~Program() noexcept
{
    glDeleteProgram(_program);
}

void gkom::Program::set(const std::string &name, int value)
{
    auto loc = glGetUniformLocation(_program, name.data());
    glUniform1i(loc, value);
}

void gkom::Program::set(const std::string &name, float value)
{
    auto loc = glGetUniformLocation(_program, name.data());
    glUniform1f(loc, value);
}

void gkom::Program::set(const std::string &name, const glm::vec2 &value)
{
    auto loc = glGetUniformLocation(_program, name.data());
    glUniform2fv(loc, 1, glm::value_ptr(value));
}

void gkom::Program::set(const std::string &name, const glm::vec3 &value)
{
    auto loc = glGetUniformLocation(_program, name.data());
    glUniform3fv(loc, 1, glm::value_ptr(value));
}

void gkom::Program::set(const std::string &name, const glm::vec4 &value)
{
    auto loc = glGetUniformLocation(_program, name.data());
    glUniform4fv(loc, 1, glm::value_ptr(value));
}

void gkom::Program::set(const std::string &name, const glm::mat2 &value)
{
    auto loc = glGetUniformLocation(_program, name.data());
    glUniformMatrix2fv(loc, 1, GL_FALSE, glm::value_ptr(value));
}

void gkom::Program::set(const std::string &name, const glm::mat3 &value)
{
    auto loc = glGetUniformLocation(_program, name.data());
    glUniformMatrix3fv(loc, 1, GL_FALSE, glm::value_ptr(value));
}

void gkom::Program::set(const std::string &name, const glm::mat4 &value)
{
    auto loc = glGetUniformLocation(_program, name.data());
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(value));
}

gkom::Program &gkom::Program::use()
{
    glUseProgram(_program);
    return *this;
}
