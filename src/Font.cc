// Copyright (c) 2018 Artur Świgoń <aswigon@aswigon.pl>

#include <stdexcept>
#include <string>
#include <unordered_map>

#include <ft2build.h>
#include FT_FREETYPE_H

#include "Font.h"
#include "Program.h"
#include "Resource.h"

namespace
{
    FT_Library _library = nullptr;
}

gkom::Font::Font(const std::string &name, std::size_t size, gkom::Program &program)
    : _program{program}, _size{size}
{
    if (!_library)
        throw std::logic_error("Please call gkom::Font::setLibrary before creating fonts");

    gkom::Resource file(name);
    auto err = FT_New_Memory_Face(_library, static_cast<FT_Byte *>(file.data()), file.size(), 0, &_face);

    if (err)
        throw std::runtime_error("Cannot open font: " + file.name());

    FT_Set_Pixel_Sizes(_face, 0, static_cast<FT_UInt>(size));

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    // Load first 128 characters of ASCII set
    for (GLubyte c = 0; c < 128; c++)
    {
        // Load character glyph
        if (FT_Load_Char(_face, c, FT_LOAD_RENDER))
            throw std::runtime_error("Cannot load glyph: " + std::to_string(c));

        // Generate texture
        GLuint texture;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, _face->glyph->bitmap.width, _face->glyph->bitmap.rows,
            0, GL_RED, GL_UNSIGNED_BYTE, _face->glyph->bitmap.buffer);

        // Set texture options
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        // Now store character for later use
        Character character = {
            texture,
            glm::ivec2(_face->glyph->bitmap.width, _face->glyph->bitmap.rows),
            glm::ivec2(_face->glyph->bitmap_left, _face->glyph->bitmap_top),
            static_cast<GLuint>(_face->glyph->advance.x)
        };

        _charmap.insert(std::pair<GLchar, Character>(c, character));
    }

    glBindTexture(GL_TEXTURE_2D, 0);
    FT_Done_Face(_face);

    // Configure VAO/VBO for texture quads
    glGenVertexArrays(1, &_VAO);
    glGenBuffers(1, &_VBO);
    glBindVertexArray(_VAO);
    glBindBuffer(GL_ARRAY_BUFFER, _VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

gkom::Font::~Font() noexcept
{
    for (auto &c : _charmap)
        glDeleteTextures(1, &c.second.texid);

    glDeleteBuffers(1, &_VBO);
    glDeleteVertexArrays(1, &_VAO);
}

void gkom::Font::renderText(const std::string &text, GLfloat x, GLfloat y, glm::vec3 color)
{
    // Activate corresponding render state
    _program.use();
    _program.set("text", int{0});
    _program.set("textColor", color);
    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(_VAO);

    // Iterate through all characters
    for (auto &c : text)
    {
        Character ch = _charmap[c];

        GLfloat xpos = x + ch.bearing.x;
        GLfloat ypos = y - (ch.size.y - ch.bearing.y);

        GLfloat w = ch.size.x;
        GLfloat h = ch.size.y;

        // Update VBO for each character
        GLfloat vertices[6][4] = {
            { xpos,     ypos + h,   0.0, 0.0 },
            { xpos,     ypos,       0.0, 1.0 },
            { xpos + w, ypos,       1.0, 1.0 },

            { xpos,     ypos + h,   0.0, 0.0 },
            { xpos + w, ypos,       1.0, 1.0 },
            { xpos + w, ypos + h,   1.0, 0.0 }
        };

        // Render glyph texture over quad
        glBindTexture(GL_TEXTURE_2D, ch.texid);

        // Update content of VBO memory
        glBindBuffer(GL_ARRAY_BUFFER, _VBO);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        // Now advance cursors for next glyph
        x += (ch.advance >> 6);
    }

    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void gkom::Font::setLibrary(FT_Library library)
{
    _library = library;
}
