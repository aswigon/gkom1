// Copyright (c) 2018 Artur Świgoń <aswigon@aswigon.pl>

#ifndef GKOM1_TEXTURE_H
#define GKOM1_TEXTURE_H

#include <string>
#include <vector>

#include <GL/glew.h>

namespace gkom
{
    class Texture
    {
    public:
        Texture() = delete;
        Texture(const Texture &) = delete;
        Texture(Texture &&) noexcept = delete;

        Texture(const std::string &name);
        ~Texture() noexcept;

        Texture &operator=(const Texture &) = delete;
        Texture &operator=(Texture &&) noexcept = delete;

        GLuint id() const { return _texture; }

        int width() const { return _width; }
        int height() const { return _height; }
        int stride() const { return _stride; }
        bool alpha() const { return _alpha; }
        void *data() { return _data.data(); }

        void use(int id) const;

    private:
        GLuint _texture;
        int _width;
        int _height;
        int _stride;
        bool _alpha;
        std::vector<GLubyte> _data;
    };
}

#endif // GKOM1_TEXTURE_H
