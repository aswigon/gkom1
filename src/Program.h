// Copyright (c) 2018 Artur Świgoń <aswigon@aswigon.pl>

#ifndef GKOM1_PROGRAM_H
#define GKOM1_PROGRAM_H

#include <initializer_list>
#include <string>

#include <GL/glew.h>
#include <glm/glm.hpp>

#include "Shader.h"

namespace gkom
{
    class Program
    {
    public:
        Program() = delete;
        Program(const Program &) = delete;
        Program(Program &&) noexcept = default;

        Program(std::initializer_list<const gkom::Shader *> list);
        ~Program() noexcept;

        Program &operator=(const Program &) = delete;
        Program &operator=(Program &&) noexcept = default;

        GLuint id() const { return _program; }

        void set(const std::string &name, int value);
        void set(const std::string &name, float value);
        void set(const std::string &name, const glm::vec2 &value);
        void set(const std::string &name, const glm::vec3 &value);
        void set(const std::string &name, const glm::vec4 &value);
        void set(const std::string &name, const glm::mat2 &value);
        void set(const std::string &name, const glm::mat3 &value);
        void set(const std::string &name, const glm::mat4 &value);

        Program &use();

    private:
        GLuint _program;
    };
}

#endif // GKOM1_PROGRAM_H
