// Copyright (c) 2018 Artur Świgoń <aswigon@aswigon.pl>

#include <array>
#include <cmath>
#include <mutex>

#include <GL/glew.h>
#include <glm/glm.hpp>

#include "Cylinder.h"

namespace
{
    const std::size_t NFACES = 32;
    const float ANGLESTEP = 360.0f / NFACES;
    std::once_flag onceFlag;

    std::array<float, (2 * NFACES + 2) * 5> vertices{};

    std::array<unsigned, 4 * NFACES * 3> indices{};
}

static_assert(NFACES % 2 == 0, "Number of cylinder faces must be even");

gkom::Cylinder::Cylinder(gkom::Program &program)
    : _program{program}
{
    std::call_once(onceFlag, []{
        // Generate vertex data

        float angle = 0.0f;

        std::size_t baseV;
        std::size_t baseI;

        float y1 = +0.5f;
        float y2 = -0.5f;
        float v1 = 1.0f;
        float v2 = 0.0f;

        for (std::size_t i = 0; i < NFACES; ++i)
        {
            baseV = 10 * i;
            baseI = 12 * i;

            float x = std::cos(glm::radians(angle));
            float z = std::sin(glm::radians(angle));
            float u = (i % 2 == 0) ? 0.0f : 1.0f;

            vertices[baseV + 0] = x;
            vertices[baseV + 1] = y1;
            vertices[baseV + 2] = z;
            vertices[baseV + 3] = u;
            vertices[baseV + 4] = v1;

            vertices[baseV + 5] = x;
            vertices[baseV + 6] = y2;
            vertices[baseV + 7] = z;
            vertices[baseV + 8] = u;
            vertices[baseV + 9] = v2;

            auto j = static_cast<unsigned>(i);
            auto k = 2 * static_cast<int>(NFACES);

            indices[baseI + 0] = (2 * j + 2) % k;
            indices[baseI + 1] = (2 * j + 0) % k;
            indices[baseI + 2] = (2 * j + 1) % k;

            indices[baseI + 3] = (2 * j + 2) % k;
            indices[baseI + 4] = (2 * j + 1) % k;
            indices[baseI + 5] = (2 * j + 3) % k;

            indices[baseI + 6] = (2 * j + 2) % k;
            indices[baseI + 7] = 2 * NFACES;
            indices[baseI + 8] = (2 * j + 0) % k;

            indices[baseI + 9] = (2 * j + 3) % k;
            indices[baseI + 10] = (2 * j + 1) % k;
            indices[baseI + 11] = 2 * NFACES + 1;

            angle += ANGLESTEP;
        }

        baseV = 10 * NFACES;
        baseI = 12 * NFACES;

        vertices[baseV + 0] = 0.0f;
        vertices[baseV + 1] = y1;
        vertices[baseV + 2] = 0.0f;
        vertices[baseV + 3] = 0.5f; // FIXME
        vertices[baseV + 4] = v1;

        vertices[baseV + 5] = 0.0f;
        vertices[baseV + 6] = y2;
        vertices[baseV + 7] = 0.0f;
        vertices[baseV + 8] = 0.5f; // FIXME
        vertices[baseV + 9] = v2;
    });

    glGenBuffers(1, &_ebo);
    glGenBuffers(1, &_vbo);
    glGenVertexArrays(1, &_vao);

    glBindVertexArray(_vao);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat), vertices.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), indices.data(), GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), reinterpret_cast<void *>(0));
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), reinterpret_cast<void *>(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);
}

gkom::Cylinder::~Cylinder() noexcept
{
    glDeleteVertexArrays(1, &_vao);
    glDeleteBuffers(1, &_vbo);
    glDeleteBuffers(1, &_ebo);
}

void gkom::Cylinder::draw()
{
    glBindVertexArray(_vao);
    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, nullptr);
}
