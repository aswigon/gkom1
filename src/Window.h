// Copyright (c) 2018 Artur Świgoń <aswigon@aswigon.pl>

#ifndef GKOM1_WINDOW_H
#define GKOM1_WINDOW_H

#include <string>

#include <GLFW/glfw3.h>

namespace gkom
{
    class Window
    {
    public:
        Window() = delete;
        ~Window() noexcept = delete;

        static void init(GLFWwindow *window, int width, int height);
        static void mainLoop();
        static void title(const std::string &title);
        static void show();

        static int getWidth();
        static int getHeight();
    };
}

#endif // GKOM1_WINDOW_H
