// Copyright (c) 2018 Artur Świgoń <aswigon@aswigon.pl>

#ifndef GKOM1_FONT_H
#define GKOM1_FONT_H

#include <string>
#include <unordered_map>

#include <ft2build.h>
#include FT_FREETYPE_H
#include <GL/glew.h>
#include <glm/glm.hpp>

#include "Program.h"

namespace gkom
{
    class Font
    {
    public:
        Font() = delete;
        Font(const Font &) = delete;
        Font(Font &&) noexcept = delete;

        Font(const std::string &name, std::size_t size, gkom::Program &program);
        ~Font() noexcept;

        Font &operator=(const Font &) = delete;
        Font &operator=(Font &&) noexcept = delete;

        void renderText(const std::string &text, GLfloat x, GLfloat y, glm::vec3 color);

        gkom::Program &program() const { return _program; }
        std::size_t size() const { return _size; }

        static void setLibrary(FT_Library library);

    private:
        struct Character
        {
            GLuint texid;
            glm::ivec2 size;
            glm::ivec2 bearing;
            GLuint advance;
        };

        std::size_t _size;
        gkom::Program &_program;
        FT_Face _face;
        std::unordered_map<GLubyte, Character> _charmap;
        GLuint _VAO;
        GLuint _VBO;
    };
}

#endif // GKOM1_FONT_H
