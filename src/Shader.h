// Copyright (c) 2018 Artur Świgoń <aswigon@aswigon.pl>

#ifndef GKOM1_SHADER_H
#define GKOM1_SHADER_H

#include <string>

#include <GL/glew.h>

namespace gkom
{
    enum class ShaderType
    {
        VERTEX,
        TESSELATION_CONTROL,
        TESSELATION_EVALUATION,
        GEOMETRY,
        FRAGMENT,
        COMPUTE,
    };

    class Shader
    {
    public:
        Shader() = delete;
        Shader(const Shader &) = delete;
        Shader(Shader &&) noexcept = delete;

        Shader(const std::string &name, ShaderType type);
        ~Shader() noexcept;

        Shader &operator=(const Shader &) = delete;
        Shader &operator=(Shader &&) noexcept = delete;

        GLuint id() const { return _shader; }

    private:
        GLuint _shader;
    };
}

#endif // GKOM1_SHADER_H
