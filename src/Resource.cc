// Copyright (c) 2018 Artur Świgoń <aswigon@aswigon.pl>

#include <cstring>
#include <limits>
#include <stdexcept>
#include <string>
#include <vector>

#if defined __APPLE__
#include <cstdint>
#include <mach-o/dyld.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#elif defined __linux__
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#elif defined _WIN32
#include <codecvt>
#include <locale>
#include <windows.h>
#else
#error "Unsupported operating system"
#endif

#include "Resource.h"

namespace
{
#if defined __APPLE__

    const char *PATH_SEP = "/";
    const char *PAR_DIR = "..";
    const char *RES_DIR = "res";

    std::string getExecutablePath() // TODO: Test this code
    {
        std::vector<char> buf;
        std::uint32_t len = 512;
        int status;

        // _NSGetExecutablePath() sets 'len' to the required buffer size if insufficient,
        // therefore this loop should execute at most twice
        do
        {
            buf.resize(static_cast<std::size_t>(len));
            status = _NSGetExecutablePath(buf.data(), &len);
        }
        while (status);

        buf.push_back('\0');
        return std::string(buf.data());
    }

#elif defined __linux__

    const char *PATH_SEP = "/";
    const char *PAR_DIR = "..";
    const char *RES_DIR = "res";

    std::string getExecutablePath()
    {
        std::vector<char> buf;
        ssize_t len;

        // readlink() writes as many bytes as possible, so 'len == buf.size()'
        // indicates that more space is needed
        do
        {
            buf.resize(buf.size() + 512);
            len = readlink("/proc/self/exe", buf.data(), buf.size());

            if (len < 0)
                throw std::runtime_error("Cannot determine executable location");
        }
        while (buf.size() == static_cast<std::size_t>(len));

        buf[len] = '\0';
        return std::string(buf.data());
    }

#elif defined _WIN32

    const wchar_t *PATH_SEP = L"\\";
    const wchar_t *PAR_DIR = L"..";
    const wchar_t *RES_DIR = L"res";

    std::wstring getExecutablePath() // TODO: Test this code
    {
        std::vector<wchar_t> buf;
        DWORD len;

        HMODULE hModule = GetModuleHandleW(nullptr);

        // GetModuleFileNameW() writes as many characters as possible, so 'len == buf.size()'
        // indicates that more space is needed
        do
        {
            buf.resize(buf.size() + 512);
            len = GetModuleFileNameW(hModule, buf.data(), static_cast<DWORD>(buf.size()));

            if (len == 0)
                throw std::runtime_error("Cannot determine executable location");
        }
        while (buf.size() == static_cast<std::size_t>(len));

        buf[len] = '\0'; // Needed on Windows XP
        return std::wstring(buf.data());
    }

#endif

    template <typename T>
    std::basic_string<T> getResourcePath(const std::basic_string<T> &executable_path)
    {
        // Extract path where the executable is located (i.e. '/usr/bin' for '/usr/bin/prog')
        auto pos = executable_path.rfind(PATH_SEP);
        auto path = executable_path.substr(0, pos);

        path = path + PATH_SEP + PAR_DIR + PATH_SEP + RES_DIR;

        return path;
    }
}

gkom::Resource::Resource(const std::string &name)
    : _name(name)
{
    // 'static' is equivalent to initialization based on std::call_once
    static auto executablePath = getExecutablePath();
    static auto resourcePath = getResourcePath(executablePath);

#if defined _WIN32
    std::wstring_convert<std::codecvt_utf8<wchar_t>> conv;
    auto path = resourcePath + PATH_SEP + conv.from_bytes(name);

    HANDLE file = CreateFileW(path.c_str(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, 0, nullptr);
    if (file == INVALID_HANDLE_VALUE)
    {
        throw std::runtime_error("Cannot open resource file: " + conv.to_bytes(path));
    }

    LARGE_INTEGER lsize;
    BOOL status = GetFileSizeEx(file, &lsize);
    if (!status)
    {
        CloseHandle(file);
        throw std::runtime_error("Cannot determine resource file size: " + conv.to_bytes(path));
    }

    auto size = static_cast<std::size_t>(lsize.QuadPart);
    HANDLE mapping = CreateFileMappingW(file, nullptr, PAGE_READONLY, 0, 0, nullptr);
    if (mapping == nullptr)
    {
        CloseHandle(file);
        throw std::runtime_error("Cannot map resource file onto memory: " + conv.to_bytes(path));
    }

    void *addr = MapViewOfFile(mapping, FILE_MAP_READ, 0, 0, 0);
    if (addr == nullptr)
    {
        CloseHandle(mapping);
        CloseHandle(file);
        throw std::runtime_error("Cannot map resource file onto memory: " + conv.to_bytes(path));
    }

    // MapViewOfFile() holds a reference to the file and it is safe to close other handles
    CloseHandle(mapping);
    CloseHandle(file);
    _size = size;
    _address = addr;
#else
    auto path = resourcePath + PATH_SEP + name;

    int fd = open(path.c_str(), O_RDONLY);
    if (fd < 0)
    {
        throw std::runtime_error("Cannot open resource file: " + path);
    }

    struct stat buffer{};
    int status = fstat(fd, &buffer);
    if (status < 0)
    {
        close(fd);
        throw std::runtime_error("Cannot determine resource file size: " + path);
    }

    auto size = static_cast<std::size_t>(buffer.st_size);
    void *addr = mmap(nullptr, size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (addr == MAP_FAILED)
    {
        close(fd);
        throw std::runtime_error("Cannot map resource file onto memory: " + path);
    }

    // mmap() holds a reference to the file and it is safe to close the descriptor
    close(fd);
    _size = size;
    _address = addr;
#endif
}

gkom::Resource::~Resource() noexcept
{
#if defined _WIN32
    UnmapViewOfFile(_address);
#else
    munmap(_address, _size);
#endif
}

std::size_t gkom::Resource::read(void *dst, std::size_t cnt)
{
    // EOF condition
    if (_offset == _size)
        return 0;

    //if (cnt > (std::numeric_limits<std::size_t>::max() - _offset))
    //    throw std::overflow_error("Overflow");

    std::size_t new_offset = _offset + cnt;

    // Clamp to buffer size
    if (new_offset > _size)
        new_offset = _size;

    auto range = new_offset - _offset;
    auto addr = &(static_cast<char *>(_address)[_offset]);

    std::memcpy(dst, addr, range);
    _offset = new_offset;

    return range;
}
