// Copyright (c) 2018 Artur Świgoń <aswigon@aswigon.pl>

#ifndef GKOM1_BOX_H
#define GKOM1_BOX_H

#include <GL/glew.h>

#include "Program.h"

namespace gkom
{
    class Box
    {
    public:
        Box() = delete;
        Box(const Box &) = delete;
        Box(Box &&) noexcept = delete;

        Box(gkom::Program &program);
        ~Box() noexcept;

        Box &operator=(const Box &) = delete;
        Box &operator=(Box &&) noexcept = delete;

        gkom::Program &program() const { return _program; }

        void draw();

    private:
        gkom::Program &_program;
        GLuint _ebo;
        GLuint _vao;
        GLuint _vbo;
    };
}

#endif // GKOM1_BOX_H
