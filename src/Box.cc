// Copyright (c) 2018 Artur Świgoń <aswigon@aswigon.pl>

#include <array>

#include <GL/glew.h>

#include "Box.h"
#include "Program.h"

namespace
{
    std::array<float, 40> vertices = {
        -0.5f, -0.5f, -0.5f, /* */ 0.0f, 0.0f, // 0 (0, 0, 0)
        -0.5f, -0.5f, +0.5f, /* */ 1.0f, 0.0f, // 1 (0, 0, 1)
        -0.5f, +0.5f, -0.5f, /* */ 0.0f, 1.0f, // 2 (0, 1, 0)
        -0.5f, +0.5f, +0.5f, /* */ 1.0f, 1.0f, // 3 (0, 1, 1)
        +0.5f, -0.5f, -0.5f, /* */ 1.0f, 0.0f, // 4 (1, 0, 0)
        +0.5f, -0.5f, +0.5f, /* */ 0.0f, 0.0f, // 5 (1, 0, 1)
        +0.5f, +0.5f, -0.5f, /* */ 1.0f, 1.0f, // 6 (1, 1, 0)
        +0.5f, +0.5f, +0.5f, /* */ 0.0f, 1.0f, // 7 (1, 1, 1)
    };

    std::array<unsigned, 36> indices = {
        6, 2, 0,
        4, 6, 0,
        3, 2, 6,
        7, 3, 6,
        0, 1, 5,
        4, 0, 5,
        3, 1, 0,
        2, 3, 0,
        6, 4, 5,
        7, 6, 5,
        3, 7, 5,
        1, 3, 5,
    };
}

gkom::Box::Box(gkom::Program &program)
    : _program{program}
{
    glGenBuffers(1, &_ebo);
    glGenBuffers(1, &_vbo);
    glGenVertexArrays(1, &_vao);

    glBindVertexArray(_vao);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat), vertices.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), indices.data(), GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), reinterpret_cast<void *>(0));
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), reinterpret_cast<void *>(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

    //_program.use();
    //_program.set("aTexture", int{0});
}

gkom::Box::~Box() noexcept
{
    glDeleteVertexArrays(1, &_vao);
    glDeleteBuffers(1, &_vbo);
    glDeleteBuffers(1, &_ebo);
}

void gkom::Box::draw()
{
    glBindVertexArray(_vao);
    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, nullptr);
}
