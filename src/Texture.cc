// Copyright (c) 2018 Artur Świgoń <aswigon@aswigon.pl>

// PNG loading based on:
// http://pulsarengine.com/2009/01/reading-png-images-from-memory/
// https://blog.nobel-joergensen.com/2010/11/07/loading-a-png-as-texture-in-opengl-using-libpng/

#include <array>
#include <csetjmp>
#include <cstdint>
#include <cstring>
#include <stdexcept>
#include <string>
#include <vector>

#include <GL/glew.h>
#include <png.h>

#include "Resource.h"
#include "Texture.h"

namespace
{
    const std::size_t SIG_LEN = 8;

    void readData(png_structp png_ptr, png_bytep dst, png_size_t cnt)
    {
        png_voidp io_ptr = png_get_io_ptr(png_ptr);
        if (!io_ptr)
            throw std::runtime_error("Failed to obtain PNG I/O pointer");

        auto &resource = *(static_cast<gkom::Resource *>(io_ptr));
        size_t res = resource.read(static_cast<void *>(dst), static_cast<std::size_t>(cnt));

        if (static_cast<png_size_t>(res) != cnt)
            throw std::runtime_error("Failed to read data for PNG");
    }
}

gkom::Texture::Texture(const std::string &name)
{
    gkom::Resource file(name);

    auto png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
    if (!png_ptr)
        throw std::runtime_error("PNG error");

    auto info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr)
    {
        png_destroy_read_struct(&png_ptr, nullptr, nullptr);
        throw std::runtime_error("PNG error");
    }

    if (setjmp(png_jmpbuf(png_ptr)))
    {
        png_destroy_read_struct(&png_ptr, &info_ptr, nullptr);
        throw std::runtime_error("PNG error");
    }

    // Register custom read function
    png_set_read_fn(png_ptr, &file, &readData);
    png_set_sig_bytes(png_ptr, 0);

    png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_PACKING | PNG_TRANSFORM_EXPAND, nullptr);

    // Read header
    png_uint_32 width, height;
    int bit_depth, color_type;

    png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type, nullptr, nullptr, nullptr);

    _width = static_cast<int>(width);
    _height = static_cast<int>(height);
    _alpha = static_cast<bool>(color_type & PNG_COLOR_MASK_ALPHA);
    _stride = static_cast<int>(png_get_rowbytes(png_ptr, info_ptr));
    _data.resize(static_cast<std::size_t>(_stride) * static_cast<std::size_t>(_height));

    auto row_pointers = png_get_rows(png_ptr, info_ptr);

    // Read rows in reverse order
    for (std::size_t i = 0; i < _height; ++i)
        std::memcpy(_data.data() + (_stride * (_height - 1 - i)), row_pointers[i], static_cast<std::size_t>(_stride));

    // Cleanup
    png_destroy_read_struct(&png_ptr, &info_ptr, nullptr);

    // Make OpenGL texture
    // TODO: Enable setting parameters (GL_MIRRORED_REPEAT, GL_LINEAR, etc.)

    glGenTextures(1, &_texture);
    glBindTexture(GL_TEXTURE_2D, _texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _width, _height, 0,
        _alpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, _data.data());
    glGenerateMipmap(GL_TEXTURE_2D);
}

gkom::Texture::~Texture() noexcept
{
    glDeleteTextures(1, &_texture);
}

void gkom::Texture::use(int id) const
{
    GLenum texid{};

    switch (id)
    {
    case 0:
        texid = GL_TEXTURE0;
        break;
    case 1:
        texid = GL_TEXTURE1;
        break;
    case 2:
        texid = GL_TEXTURE2;
        break;
    case 3:
        texid = GL_TEXTURE3;
        break;
    case 4:
        texid = GL_TEXTURE4;
        break;
    case 5:
        texid = GL_TEXTURE5;
        break;
    case 6:
        texid = GL_TEXTURE6;
        break;
    case 7:
        texid = GL_TEXTURE7;
        break;
    case 8:
        texid = GL_TEXTURE8;
        break;
    default:
        throw std::runtime_error("Bad texture ID");
    }

    glActiveTexture(texid);
    glBindTexture(GL_TEXTURE_2D, _texture);
}
