// Copyright (c) 2018 Artur Świgoń <aswigon@aswigon.pl>

#include <array>
#include <cmath>
#include <stdexcept>
#include <sstream>
#include <string>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Box.h"
#include "Cylinder.h"
#include "Font.h"
#include "Program.h"
#include "Shader.h"
#include "Skybox.h"
#include "Texture.h"
#include "Window.h"

namespace
{
    using coord_t = struct { int x; int y; };

    bool fullscreen = false;
    GLFWmonitor *primaryMonitor;
    const GLFWvidmode *videoMode;

    GLFWwindow *windowHandle;
    coord_t windowSize;
    coord_t windowPos;
    std::string windowTitle;

    float sceneYaw = 15.0f;
    float sceneYawDelta = 0.0f;

    float scenePitch = 10.0f;
    float scenePitchDelta = 0.0f;

    float sceneZoom = 1.0f;
    float sceneZoomDelta = 0.0f;

    float machinePosition = 0.0f;
    float machineMovementDelta = 0.5f;
    bool machineMovement = false;

    float cylinderRotation = 0.0f;

    void windowCallback(GLFWwindow *window, int x, int y)
    {
        // Ignore these parameters as this is a callback common for repositioning and resizing
        (void)window;
        (void)x;
        (void)y;

        if (!fullscreen)
        {
            glfwGetWindowSize(windowHandle, &windowSize.x, &windowSize.y);
            glfwGetWindowPos(windowHandle, &windowPos.x, &windowPos.y);
        }
    }

    void framebufferCallback(GLFWwindow *window, int x, int y)
    {
        (void)window;

        glViewport(0, 0, x, y);
    }

    void keyCallback(GLFWwindow *window, int key, int code, int action, int mods)
    {
        (void)window;
        (void)code;

        GLFWmonitor *monitor{};

        // Handle key presses
        if (!mods && action == GLFW_PRESS) switch (key)
        {
        case GLFW_KEY_F11:
            if (fullscreen)
            {
                glfwSetWindowMonitor(windowHandle, nullptr,
                    windowPos.x, windowPos.y, windowSize.x, windowSize.y, GLFW_DONT_CARE);
            }
            else
            {
                monitor = primaryMonitor;

                glfwSetWindowMonitor(windowHandle, monitor,
                    0, 0, videoMode->width, videoMode->height, videoMode->refreshRate);
            }

            fullscreen = !fullscreen;
            break;

        case GLFW_KEY_A:
        case GLFW_KEY_D:
            if (sceneYawDelta)
                sceneYawDelta = 0.0f;
            else
                sceneYawDelta = (key == GLFW_KEY_A) ? -1.0f : 1.0f;
            break;

        case GLFW_KEY_W:
        case GLFW_KEY_S:
            if (scenePitchDelta)
                scenePitchDelta = 0.0f;
            else
                scenePitchDelta = (key == GLFW_KEY_W) ? 1.0f : -1.0f;
            break;

        case GLFW_KEY_SPACE:
            machineMovement = !machineMovement;
            break;

        default:
            break;
        }
        else if (!mods && action == GLFW_RELEASE) switch (key)
        {
        case GLFW_KEY_A:
        case GLFW_KEY_D:
            sceneYawDelta = 0.0f;
            break;

        case GLFW_KEY_W:
        case GLFW_KEY_S:
            scenePitchDelta = 0.0f;
            break;

        default:
            break;
        }
    }

    void scrollCallback(GLFWwindow *window, double x, double y)
    {
        (void)window;
        (void)x;

        sceneZoomDelta = static_cast<float>(y) * 0.5f;
    }
}

void gkom::Window::init(GLFWwindow *window, int width, int height)
{
    static bool initialized = false;

    if (initialized)
        return;
    else
        initialized = true;

    primaryMonitor = glfwGetPrimaryMonitor();
    videoMode = glfwGetVideoMode(primaryMonitor);

    int minWidth = 800;
    int minHeight = 600;
    int maxWidth = GLFW_DONT_CARE;
    int maxHeight = GLFW_DONT_CARE;

    // Determine the window size
    windowSize.x = (width > minWidth) ? width : minWidth;
    windowSize.y = (height > minHeight) ? height : minHeight;

    // Try to center the window
    windowPos.x = (videoMode->width - windowSize.x) / 2;
    windowPos.y = (videoMode->height - windowSize.y) / 2;
    //windowPos.x = 0;
    //windowPos.y = 0;

    // Set windows parameters
    windowHandle = window;
    glfwSetWindowSize(windowHandle, windowSize.x, windowSize.y);
    glfwSetWindowPos(windowHandle, windowPos.x, windowPos.y);
    glfwSetWindowSizeLimits(windowHandle, minWidth, minHeight, maxWidth, maxHeight);

    // Register callbacks
    glfwSetWindowSizeCallback(windowHandle, &windowCallback);
    glfwSetFramebufferSizeCallback(windowHandle, &framebufferCallback);
    glfwSetKeyCallback(windowHandle, &keyCallback);
    glfwSetScrollCallback(windowHandle, &scrollCallback);
    glfwSetWindowPosCallback(windowHandle, &windowCallback);
}

void gkom::Window::mainLoop()
{
    gkom::Shader vertShader("basic.vert", gkom::ShaderType::VERTEX);
    gkom::Shader fragShader("basic.frag", gkom::ShaderType::FRAGMENT);
    gkom::Program testProgram{&vertShader, &fragShader};

    gkom::Texture metalTexture("metal.png");
    gkom::Texture dirtTexture("dirt.png");
    gkom::Texture stoneTexture("stone.png");
    gkom::Texture bricksTexture("bricks.png");

    gkom::Shader textVertShader("text.vert", gkom::ShaderType::VERTEX);
    gkom::Shader textFragShader("text.frag", gkom::ShaderType::FRAGMENT);
    gkom::Program textProgram{&textVertShader, &textFragShader};
    gkom::Font fontLight("FiraSans-Light.otf", 32, textProgram);
    gkom::Font fontRegular("FiraSans-Regular.otf", 16, textProgram);

    gkom::Shader skyboxVertShader("skybox.vert", gkom::ShaderType::VERTEX);
    gkom::Shader skyboxFragShader("skybox.frag", gkom::ShaderType::FRAGMENT);
    gkom::Program skyboxProgram{&skyboxVertShader, &skyboxFragShader};
    std::array<std::string, 6> skyboxFaces = {
        "skybox-right.png", "skybox-left.png",
        "skybox-bottom.png", "skybox-top.png",
        "skybox-front.png", "skybox-back.png"
    };
    gkom::Skybox skybox(skyboxFaces, skyboxProgram);

    gkom::Box testBox(testProgram);
    gkom::Cylinder testCylinder(testProgram);

    gkom::Box testPlate(testProgram);
    gkom::Box testGround(testProgram);

    glEnable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    auto lastTime = static_cast<GLfloat>(glfwGetTime());
    auto time = 0.0f;
    auto timeDelta = 0.0f;

    auto fps = 0.0f;
    auto lastFpsTime = lastTime;

    while (!glfwWindowShouldClose(windowHandle))
    {
        lastTime = time;
        time = static_cast<GLfloat>(glfwGetTime());
        timeDelta = (time - lastTime) * 50.0f;

        // Handle scene yaw (camera moving around)
        sceneYaw += sceneYawDelta * timeDelta;
        if (sceneYaw >= 360.0f)
            sceneYaw = 0.0f;
        else if (sceneYaw <= -0.0f)
            sceneYaw = 359.0f;

        // Handle scene pitch (camera moving up and down)
        scenePitch += scenePitchDelta * timeDelta;
        if (scenePitch >= 88.0f)
            scenePitch = 88.0f;
        else if (scenePitch <= -0.0f)
            scenePitch = 0.0f;

        // Handle smooth scene zooming
        auto sceneZoomDiff = timeDelta * 0.1f * sceneZoomDelta;
        sceneZoomDelta -= sceneZoomDiff;
        sceneZoom += sceneZoomDiff;
        if (abs(sceneZoomDelta) <= 0.1f)
            sceneZoomDelta = 0.0f;

        if (sceneZoom <= 0.2f)
            sceneZoom = 0.2f;
        else if (sceneZoom >= 2.0f)
            sceneZoom = 2.0f;

        // Handle machine movement
        if (machineMovement)
            machinePosition += machineMovementDelta * timeDelta * 0.05f;

        if (machinePosition > 2.0f)
        {
            machinePosition = 2.0f;
            machineMovementDelta = -1.0f;
        }
        else if (machinePosition < -2.0f)
        {
            machinePosition = -2.0f;
            machineMovementDelta = 1.0f;
        }

        glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        float camR = 5.0f / sceneZoom; //5.0f;
        float camX = std::cos(glm::radians(scenePitch)) * std::sin(glm::radians(sceneYaw)) * camR;
        float camY = std::sin(glm::radians(scenePitch)) * camR;
        float camZ = std::cos(glm::radians(scenePitch)) * std::cos(glm::radians(sceneYaw)) * camR;
        glm::mat4 view = glm::lookAt(glm::vec3(camX, camY, camZ), glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));

        glm::mat4 projection = glm::perspective(glm::radians(45.0f), (float)windowSize.x / (float)windowSize.y,
            0.1f, 100.0f);

        skybox.program().use();
        skybox.program().set("projection", projection);
        skybox.program().set("view", glm::mat4(glm::mat3(view)));
        skybox.draw();

        // Parameters
        const float wallDistance = 20.0f;
        const float cylinderDistance = 0.8f;
        const glm::vec3 cylinderScale(0.4f, 1.88f, 0.4f);

        cylinderRotation = machinePosition / cylinderScale.x;

        glm::mat4 modelP1 = glm::scale(glm::mat4{1.0f}, glm::vec3(0.1f, 2.0f, 0.5f));
        modelP1 = glm::translate(modelP1, glm::vec3(0.5f * wallDistance, 0.0f, 0.0f));

        glm::mat4 modelP2 = glm::translate(modelP1, glm::vec3(-wallDistance, 0.0f, 0.0f));

        glm::mat4 modelC1{1.0f};
        modelC1 = glm::translate(modelC1, glm::vec3(0.0f, 0.5f * cylinderDistance, 0.0f));
        modelC1 = glm::rotate(modelC1, -cylinderRotation, glm::vec3(1.0f, 0.0f, 0.0f));
        modelC1 = glm::rotate(modelC1, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        modelC1 = glm::scale(modelC1, cylinderScale);

        glm::mat4 modelC2 = glm::translate(glm::mat4{1.0f}, glm::vec3(0.0f, -0.5f * cylinderDistance, 0.0f));
        modelC2 = glm::rotate(modelC2, cylinderRotation, glm::vec3(1.0f, 0.0f, 0.0f));
        modelC2 = glm::rotate(modelC2, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        modelC2 = glm::scale(modelC2, cylinderScale);

        glm::mat4 modelG1 = glm::translate(glm::mat4{1.0f}, glm::vec3(0.0f, -1.0f, 0.0f));
        modelG1 = glm::scale(modelG1, glm::vec3(5.0f, 0.1f, 5.0f));
        modelG1 = glm::rotate(modelG1, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));

        glm::mat4 modelX1{1.0f};
        modelX1 = glm::translate(modelX1, glm::vec3(0.0f, 0.0f, machinePosition));
        modelX1 = glm::scale(modelX1, glm::vec3(1.8f, 0.04f, 4.0f));
        modelX1 = glm::rotate(modelX1, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));

        metalTexture.use(0);
        dirtTexture.use(1);
        stoneTexture.use(2);
        bricksTexture.use(3);

        testBox.program().use();
        testBox.program().set("aTexture", int{3});
        testBox.program().set("model", modelP1);
        testBox.program().set("view", view);
        testBox.program().set("projection", projection);
        testBox.draw();

        testBox.program().set("model", modelP2);
        testBox.draw();

        testCylinder.program().use();
        testCylinder.program().set("aTexture", int{2});
        testCylinder.program().set("model", modelC1);
        testCylinder.program().set("view", view);
        testCylinder.program().set("projection", projection);
        testCylinder.draw();

        testCylinder.program().set("model", modelC2);
        testCylinder.draw();

        testGround.program().use();
        testGround.program().set("aTexture", int{1});
        testGround.program().set("model", modelG1);
        testGround.program().set("view", view);
        testGround.program().set("projection", projection);
        testGround.draw();

        testPlate.program().use();
        testPlate.program().set("aTexture", int{0});
        testPlate.program().set("model", modelX1);
        testPlate.program().set("view", view);
        testPlate.program().set("projection", projection);
        testPlate.draw();

        glm::mat4 textProjection = glm::ortho(0.0f, static_cast<GLfloat>(windowSize.x),
            0.0f, static_cast<GLfloat>(windowSize.y));

        // Recalculate FPS every second
        if (time - lastFpsTime >= 1.0f)
        {
            fps = 1.0f / (time - lastTime);
            lastFpsTime = time;
        }

        std::stringstream info_msg;
        info_msg << "FPS: ";
        info_msg << static_cast<int>(fps);

        fontLight.program().use().set("projection", textProjection);
        fontLight.renderText(info_msg.str(), 25.0f, 25.0f, glm::vec3(1.0f, 1.0f, 1.0f));

        fontRegular.program().use().set("projection", textProjection);
        fontRegular.renderText("Copyright (c) 2018  Artur Swigon  <aswigon@aswigon.pl>",
            windowSize.x - 26* fontRegular.size(),
            windowSize.y - fontRegular.size() - 15.0f,
            glm::vec3(1.0f, 1.0f, 1.0f));

        // Swap buffers
        glfwSwapBuffers(windowHandle);
        glfwPollEvents();
    }
}

void gkom::Window::title(const std::string &title)
{
    windowTitle = title;

    glfwSetWindowTitle(windowHandle, windowTitle.c_str());
}

void gkom::Window::show()
{
    glfwShowWindow(windowHandle);
    glfwFocusWindow(windowHandle);
}

int gkom::Window::getWidth()
{
    return windowSize.x;
}

int gkom::Window::getHeight()
{
    return windowSize.y;
}
