// Copyright (c) 2018 Artur Świgoń <aswigon@aswigon.pl>

#ifndef GKOM1_RESOURCE_H
#define GKOM1_RESOURCE_H

#include <cstddef>
#include <string>

namespace gkom
{
    class Resource
    {
    public:
        Resource() = delete;
        Resource(const Resource &) = delete;
        Resource(Resource &&) noexcept = delete;

        Resource(const std::string &name);
        ~Resource() noexcept;

        Resource &operator=(const Resource &) = delete;
        Resource &operator=(Resource &&) noexcept = delete;

        void *data() const { return _address; }
        std::size_t size() const { return _size; }
        std::size_t read(void *dst, std::size_t cnt);
        std::string name() const { return _name; }

    private:
        std::string _name;
        std::size_t _offset = 0;
        std::size_t _size;
        void *_address;
    };
}

#endif // GKOM1_RESOURCE_H
