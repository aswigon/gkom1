// Copyright (c) 2018 Artur Świgoń <aswigon@aswigon.pl>

#ifndef GKOM1_SKYBOX_H
#define GKOM1_SKYBOX_H

#include <array>
#include <string>

#include <GL/glew.h>

#include "Program.h"

namespace gkom
{
    class Skybox
    {
    public:
        Skybox() = delete;
        Skybox(const Skybox &) = delete;
        Skybox(Skybox &&) noexcept = delete;

        Skybox(const std::array<std::string, 6> &faces, gkom::Program &program);
        ~Skybox() noexcept;

        Skybox &operator=(const Skybox &) = delete;
        Skybox &operator=(Skybox &&) noexcept = delete;

        GLuint id() const { return _texture; }
        gkom::Program &program() const { return _program; }

        void draw();

    private:
        GLuint _texture;
        GLuint _vao;
        GLuint _vbo;
        gkom::Program &_program;
    };
}

#endif // GKOM1_SKYBOX_H
