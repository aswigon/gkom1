// Copyright (c) 2018 Artur Świgoń <aswigon@aswigon.pl>

#include <array>
#include <stdexcept>
#include <string>

#include <GL/glew.h>

#include "Resource.h"
#include "Shader.h"

gkom::Shader::Shader(const std::string &name, gkom::ShaderType type)
{
    gkom::Resource file(name);
    std::array<GLchar *, 1> strings{static_cast<GLchar *>(file.data())};
    std::array<GLint, 1> lengths{static_cast<GLint>(file.size())};

    GLenum shaderType;

    switch (type)
    {
    case gkom::ShaderType::VERTEX:
        shaderType = GL_VERTEX_SHADER;
        break;
    case gkom::ShaderType::FRAGMENT:
        shaderType = GL_FRAGMENT_SHADER;
        break;
    default:
        throw std::runtime_error("Unknown shader type");
    }

    auto shader = glCreateShader(shaderType);
    glShaderSource(shader, 1, strings.data(), lengths.data());
    glCompileShader(shader);

    GLint success;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        const std::size_t bufferSize = 512;
        std::array<GLchar, bufferSize> log{};
        GLsizei len;

        glGetShaderInfoLog(shader, bufferSize, &len, log.data());
        std::string msg(static_cast<char *>(log.data()), static_cast<std::size_t>(len));
        throw std::runtime_error("Failed to compile shader:\n" + msg);
    }

    _shader = shader;
}

gkom::Shader::~Shader() noexcept
{
    glDeleteShader(_shader);
}
